from flask import Flask, render_template, flash, redirect, url_for, session, logging,request, jsonify
from flask_mysqldb import MySQL
from wtforms import Form, SubmitField, StringField, IntegerField, DecimalField, TextField, SelectField, RadioField, TextAreaField, PasswordField, FormField, FileField, validators
from wtforms.fields.html5 import DateField, EmailField
from wtforms.validators import Required
import os
from werkzeug.utils import secure_filename
from functools import wraps
from flask_wtf import FlaskForm
from flask_wtf.file import FileField
import datetime
from datetime import timedelta

app = Flask(__name__)

UPLOAD_FOLDER = './static/img'
ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

#config MySQL
app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'csc322'
app.config['MYSQL_PASSWORD'] = 'csc322'
app.config['MYSQL_DB'] = 'simplemy'
app.config['MYSQL_CURSORCLASS'] = 'DictCursor'

#init MySQL
mysql = MySQL(app)

#list all categories
categories = [('Appliances', 'Applicances'), ('Apps & Games', 'Apps & Games'), \
            ('Arts, Crafts & Sewing', 'Arts, Crafts & Sewing'), ('Baby', 'Baby'), \
            ('Beauty & Personal Care', 'Beauty & Personal Care'), ('Books', 'Books'),
            ('Cell Phones & Accessories', 'Cell Phones & Accessories'), \
            ('Clothing, Shoes & Jewelry', 'Clothing, Shoes & Jewelry'), ('Computers', 'Computers'), \
            ('Electronics', 'Electronics'), ('Garden & Outdoor', 'Garden & Outdoor'),\
            ('Health, Household & Baby Care', 'Health, Household & Baby Care'), \
            ('Home & Kitchen', 'Home & Kitchen'), ('Luggage & Travel Gear', 'Luggage & Travel Gear'), \
            ('Musical Instruments', 'Musical Instruments'), ('Office Products', 'Office Products'), \
            ('Pet Supplies', 'Pet Supplies'), ('Sports & Outdoor', 'Sports & Outdoor'), \
            ('Vehicles', 'Vehicles')]

# list all the states
state_choices = [('AL', 'Alabama'), ('AK', 'Alaska'), ('AZ', 'Arizona'), ('AR', 'Arkansas'), \
                ('CA', 'California'), ('CO', 'Colorado'), ('CT', 'Connecticut'), \
                ('DE', 'Delaware'), ('FL', 'Florida'), ('GA', 'Georgia'), ('HI', 'Hawaii'), \
                ('ID', 'Idaho'), ('IL', 'Illinois'), ('IN', 'Indiana'), ('IA', 'Iowa'), \
                ('KS', 'Kansas'), ('KY', 'Kentucky'), ('LA', 'Louisiana'), ('ME', 'Maine'), \
                ('MD', 'Maryland'), ('MA', 'Massachusetts'), ('MI', 'Michigan'), ('MN', 'Minnesota'), \
                ('MS', 'Mississippi'), ('MO', 'Missouri'), ('MT', 'Montana'), ('NE', 'Nebraska'), \
                ('NV', 'Nevada'), ('NH', 'New Hampshire'), ('NJ', 'New Jersey'), ('NY', 'New York'), \
                ('NC', 'North Carolina'), ('ND', 'North Dakota'), ('OH', 'Ohio'), ('OK', 'Oklahoma'), \
                ('OR', 'Oregon'), ('PA', 'Pennsylvania'), ('RI', 'Rhode Island'), ('SC', 'South Carolina'), \
                ('SD', 'South Dakota'), ('TN', 'Tennessee'), ('TX', 'Texas'), ('UT', 'Utah'), \
                ('VT', 'Vermont'), ('VA', 'Virginia'), ('WA', 'Washington'), ('WV', 'West Virginia'), \
                ('WI', 'Wisconsin'), ('WY', 'Wyoming')]

class SearchForm(Form):
    search = StringField('Search', [validators.DataRequired()])
    categories = SelectField('Categories', choices=categories)

# search bar in the home page. Not finished..
@app.route('/', methods=['GET', 'POST'])
def index():
    form = SearchForm(request.form)
    if request.method == 'POST' and form.validate():
        search = form.search.data
        categories = form.categories.data
        return render_template('products.html')
    return render_template('index.html', form=form)

@app.route('/products')
def products():
    form = SearchForm(request.form)
    print(request.args)
    search = request.args.get('search')
    search = '\'%' + search + '%\''
    param = request.args.get('filter')

    cur = mysql.connection.cursor()
    #cur.execute("SELECT id, title, categories, price FROM Products WHERE %s LIKE %s" % (param, search))
    cur.execute("SELECT * FROM Products "
                "WHERE %s LIKE %s AND status = 'selling'" % (param, search))
    products = cur.fetchall()
    cur.close()

    return render_template('products.html', products=products, form=form)

@app.route('/products/<int:product_id>', methods=['POST', 'GET'])
def show_product(product_id):
    if request.method == 'POST':
        cur = mysql.connection.cursor()
        if request.form['submit'] == 'Buy Now!':
            query = "UPDATE Products SET status = 'pending' WHERE id = %s"
            cur.execute(query, (product_id,))
            mysql.connection.commit()

            query = "UPDATE fixed_price SET buyer = %s WHERE id = %s"
            cur.execute(query, (session['id'] ,product_id))
            mysql.connection.commit()
            return redirect('/')
        else:
            #retrieve data
            query = "SELECT * FROM ranged_price WHERE id = %s"
            cur.execute(query, (product_id,))
            product = cur.fetchone()

            #get bid data
            bid = request.form['bidPrice']
            bid = float(bid)

            #check bid data
            if bid < product['min_price']:
                flash('Too low bid', 'danger')
                return render_template('productRanged.html', product=product)
            if bid > product['max_price']:
                flash('Too high bid', 'danger')
                return render_template('productRanged.html', product=product)
            if bid > product['highestBid']:
                query = "UPDATE ranged_price SET highestBid = %s, secondHighestBid = %s WHERE id = %s"
                cur.execute(query, (bid, product['secondHighestBid'], product_id, ))
                mysql.connection.commit()

            #retrieve updated info
            cur.execute("SELECT * FROM Products WHERE id = %s", (product_id,))
            product = cur.fetchall()
            return render_template('productRanged.html', product=product)
        cur.close()
    else:
        cur = mysql.connection.cursor()
        query = ("SELECT price_option FROM Products WHERE id = %s")
        cur.execute(query, (product_id,))
        product = cur.fetchone()

        if product['price_option'] == 'Fixed':
            query = ("SELECT * FROM Products p "
                     "INNER JOIN fixed_price f ON p.id = f.id "
                     "WHERE p.id = %s")
            cur.execute(query, (product_id,))
            product = cur.fetchone()
            if product['status'] == 'pending':
                flash('Product is pending.', 'danger')
            return render_template('productFixed.html', product=product)
        else:
            query = ("SELECT * FROM Products p INNER JOIN ranged_price r ON p.id = r.id "
                     "WHERE p.id = %s")
            cur.execute(query, (product_id,))
            product = cur.fetchone()
            return render_template('productRanged.html', product=product)

        cur.close()

class SignUpForm(Form):
    firstname = StringField('First Name', [
        validators.Length(min=1, max=20), validators.DataRequired()])
    lastname  = StringField('Last Name', [
        validators.Length(min=1, max=20), validators.DataRequired()])
    username = StringField('Username', [
        validators.Length(min=1, max=20), validators.DataRequired()])
    email = EmailField('Email', [validators.Email(), validators.DataRequired()])
    password = PasswordField('Password', [validators.DataRequired(),
        validators.EqualTo('confirm', message='Passwords do not match')])
    confirm = PasswordField('Confirm Password')
    phone_number = StringField('Phone Number', [
        validators.Length(min=8, max=20),
        validators.DataRequired()])
    credit_card_number = StringField('Credit Card Number', [
        validators.Length(min=15, max=20), validators.DataRequired()])
    cvc = StringField('CVC', [
        validators.Length(min=3, max=3), validators.DataRequired()])
    expiration = StringField('Expiration Date', [validators.DataRequired()])
    address = StringField('Address', [validators.Length(min=5, max=50)])
    city = StringField('City', [validators.DataRequired()])
    state = SelectField('State', choices=state_choices)
    zipcode = StringField('Zipcode', [validators.Length(min=5, max=5), validators.DataRequired()])

@app.route('/SignUp', methods=['GET', 'POST'])
def SignUp():
    form = SignUpForm(request.form)
    if request.method == 'POST' and form.validate():
        firstname = check_taboo(form.firstname.data)
        lastname = check_taboo(form.lastname.data)
        username = check_taboo(form.username.data)
        email = check_taboo(form.email.data)
        password = form.password.data
        phone_number = form.phone_number.data
        address = form.address.data
        city = form.city.data
        state = form.state.data
        zipcode = form.zipcode.data
        credit_card_number = form.credit_card_number.data

        cur = mysql.connection.cursor()
        result = cur.execute("SELECT * FROM blocked_user WHERE firstname = %s \
                            AND lastname = %s", (firstname, lastname))
        if result > 0:
            flash('Your application is rejected.', 'danger')
            return redirect(url_for('login'))
        else:
            cur = mysql.connection.cursor()

            cur.execute("INSERT INTO users(username, password, permission) \
                        VALUES(%s, %s, %s)", (username, password, 2))

            cur.execute("SELECT id FROM users WHERE username = %s", [username])
            user_id = cur.fetchone()

            cur.execute("INSERT INTO userProfile(id, firstname, lastname, email, \
                        phone_number, credit_card_number, address, city, state, zipcode) \
                        VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", (user_id.get('id'), \
                        firstname, lastname, email, phone_number, credit_card_number, \
                        address, city, state, zipcode))
            
            #commit to DB
            mysql.connection.commit()
            cur.close()

            flash('Your application is proccessing. Please wait...', 'success')
            return redirect(url_for('login'))

    return render_template('signup.html', form=form)

#user login
@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        #get form field
        username = request.form['username']
        password_candidate = request.form['password']

        #create cursor
        cur = mysql.connection.cursor()
        
        #get user by id
        cur.execute('SELECT * FROM users WHERE username = %s', [username])
        user = cur.fetchone()
        if user:
            #get stored data
            password = user['password']
            session['permission'] = user['permission']

            cur.execute("SELECT * FROM user_status WHERE id = %s", [user['id']])
            user_status = cur.fetchone()
            
            #compare passowrd
            if password_candidate == password and session['permission'] < 2:
                #passed
                session['logged_in'] = True
                session['username'] = username
                session['id'] = user['id']

                if user_status and user_status['status'] == 'suspended':
                    return render_template('suspension.html')
                
                elif user_status and user_status['status'] == 'appealed':
                    flash('Please wait for appeal to be processed!', 'danger')
                    return render_template('login.html')

                else:
                    flash('You are now logged in', 'success')
                    return redirect(url_for('dashboard')) 
            else:
                error = 'Invalid login'
                return render_template('login.html', error=error)
        else:
            error = 'Username not found'
            return render_template('login.html', error=error)
        cur.close()
    return render_template('login.html')

#check if user logged in
def is_logged_in(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if 'logged_in' in session:
            return f(*args, **kwargs)
        else:
            flash('Unauthorized. Please log in', 'danger')
            return redirect(url_for('login'))
    return wrap

#logout
@app.route('/logout')
def logout():
    session.clear()
    flash('You are now logged out', 'success')
    return redirect(url_for('login'))

#dashboard
@app.route('/dashboard')
@is_logged_in
def dashboard():
    return render_template('dashboard.html')

# A GU applies to become an OU
@app.route('/process_application')
@is_logged_in
def process_application():
    cur = mysql.connection.cursor()
    
    cur.execute("SELECT * FROM users NATURAL JOIN userprofile where users.id = userprofile.id")
 
    applications = cur.fetchall()
    cur.close()
    return render_template('process_application.html', applications = applications)

#reject application
@app.route('/reject_application/<string:id>', methods=['POST'])
@is_logged_in
def reject_application(id):
    cur = mysql.connection.cursor()
    cur.execute("DELETE FROM users WHERE id = %s", (id))

    mysql.connection.commit()
    cur.close()

    flash('Application Rejected', 'success')
    return redirect(url_for('process_application'))

#approve application
@app.route('/approve_application/<string:id>', methods=['POST'])
@is_logged_in
def approve_application(id):
    cur = mysql.connection.cursor()
    cur.execute("UPDATE users SET permission = 1 WHERE id = %s", (id))
    cur.execute("INSERT INTO user_status(id, status) VALUES(%s, %s)", (id, "OU"))

    mysql.connection.commit()
    cur.close()

    flash('Application Approved', 'success')
    return redirect(url_for('process_application'))

# list all OUs 
@app.route('/list_users')
@is_logged_in
def users():
    cur = mysql.connection.cursor()
    
    result = cur.execute("SELECT id, username FROM users WHERE permission = 1")
    users = cur.fetchall()
    cur.close()

    if result > 0:
        return render_template('list_users.html', users = users)
    else:
        msg = 'No Users Found'
        return render_template('dashboard.html', msg=msg)

# Delete users
@app.route('/delete_user/<string:id>', methods=['POST'])
@is_logged_in
def delete_user(id):
    cur = mysql.connection.cursor()
    cur.execute("SELECT * FROM userprofile WHERE id = %s", [id])
    user = cur.fetchone()
    cur.execute("DELETE FROM users WHERE id = %s", [id])

    cur.execute("INSERT INTO blocked_user(firstname, lastname) VALUES(%s, %s)",
                (user['firstname'], user['lastname']))

    mysql.connection.commit()
    cur.close()

    flash('User Deleted', 'success')
    return redirect(url_for('users'))

#edit personal profile
@app.route('/edit_profile', methods=['GET', 'POST'])
@is_logged_in
def edit_profile():
    form = SignUpForm(request.form)
    if request.method == 'POST' and form.validate():
        firstname = form.firstname.data
        lastname = form.lastname.data
        username = form.username.data
        email = form.email.data
        password = form.password.data
        phone_number = form.phone_number.data
        address = form.address.data
        city = form.city.data
        state = form.state.data
        zipcode = form.zipcode.data
        credit_card_number = form.credit_card_number.data
        
        cur = mysql.connection.cursor()
        cur.execute("UPDATE userprofile SET firstname = %s, lastname = %s, \
                    email = %s, phone_number = %s, credit_card_number = %s, \
                    address = %s, city = %s, state = %s, zipcode = %s WHERE id = %s", \
                    (firstname, lastname, email, phone_number, credit_card_number, \
                    address, city, state, zipcode, session['id']))
        
        cur.execute("UPDATE users SET username = %s, password = %s WHERE id = %s", \
                    (username, password, session['id']))

        mysql.connection.commit()
        cur.close()

        flash('Profile Updated', 'success')
        return redirect(url_for('dashboard')) 
    return render_template('signup.html', form=form)

# Shows personal profile
@app.route('/personal_profile')
@is_logged_in
def personal_profile():
    cur = mysql.connection.cursor()

    # Get personal profile
    cur.execute("SELECT * FROM users NATURAL JOIN userprofile NATURAL JOIN user_status \
        WHERE id = %s", [session['id']])
    profile = cur.fetchone()

    cur.close()
    return render_template('personal_profile.html', profile=profile)

# Friend list
@app.route('/list_friends')
@is_logged_in
def friends():
    cur = mysql.connection.cursor()

    result1 = cur.execute("SELECT user_second_id, relationship, username FROM user_relation \
                        INNER JOIN users ON users.id = user_relation.user_second_id \
                        WHERE user_first_id = %s", [session['id']])
    friends_second = cur.fetchall()

    result2 = cur.execute("SELECT user_first_id, relationship, username FROM user_relation \
                        INNER JOIN users ON users.id = user_relation.user_first_id \
                        WHERE user_second_id = %s", [session['id']])
    friends_first = cur.fetchall()
    cur.close()
    
    if result1 > 0 or result2 > 0:
        return render_template('list_friends.html', friends_second=friends_second, friends_first=friends_first)
    else:
        msg = 'No Friends Found'
        return render_template('list_friends.html', msg=msg)

class FriendForm(Form):
    username = StringField('Username', [validators.DataRequired()])

# Determine which one has smaller id number
def compare(session_id, id):
    if session_id < int(id): 
        user1 = session_id
        user2 = id
    else: 
        user1 = id
        user2 = session_id
    return [user1, user2]

#Add Friend
@app.route('/add_friend', methods=['GET', 'POST'])
@is_logged_in
def add_friend():
    form = FriendForm(request.form)
    if request.method == 'POST' and form.validate():
        cur = mysql.connection.cursor()

        username = form.username.data
        result = cur.execute("SELECT id FROM users WHERE username = %s", [username])
        user_second = cur.fetchone()
        cur.close()

        if result > 0:
            user = compare(session['id'], user_second['id'])

            if user[0] == session['id']: s = 'first_request_second'
            else: s = 'second_request_first'
            
            cur = mysql.connection.cursor()
            cur.execute("INSERT INTO user_relation(user_first_id, user_second_id, relationship) \
                        VALUES(%s, %s, %s)", (user[0], user[1], s))

            mysql.connection.commit()
            cur.close()

            flash('Friend requested', 'success')
            return redirect(url_for('friends'))
        else:
            error = 'User not found'
            return render_template('add_friend.html', error=error)
    return render_template('add_friend.html', form=form)

# Delete friend
@app.route('/delete_friend/<string:id>', methods=['POST'])
@is_logged_in
def delete_friend(id):
    user = compare(session['id'], id)

    cur = mysql.connection.cursor()
    cur.execute("DELETE FROM user_relation WHERE user_first_id = %s AND \
                user_second_id = %s", (user[0], user[1]))

    mysql.connection.commit()
    cur.close()

    flash('Action Success', 'success')
    return redirect(url_for('friends'))

#approve friend request
@app.route('/approve_friend/<string:id>', methods=['POST'])
@is_logged_in
def approve_friend(id):
    user = compare(session['id'], id)

    cur = mysql.connection.cursor()
    cur.execute("UPDATE user_relation SET relationship = %s WHERE user_first_id = %s and \
                user_second_id = %s", ("friend", user[0], user[1]))

    mysql.connection.commit()
    cur.close()

    flash('Approved', 'success')
    return redirect(url_for('friends'))

class MessageForm(Form):
    message = StringField('', [validators.length(min=1)],
                          render_kw={'autofocus': True})

@app.route('/chatting/<string:id>', methods=['GET', 'POST'])
@is_logged_in
def chatting(id):
    form = MessageForm(request.form)

    cur = mysql.connection.cursor()
    result = cur.execute("SELECT username FROM users WHERE id = %s", [id])
    friend = cur.fetchone
    cur.close()

    if result > 0:
        session['fid'] = id  # friend id

        if request.method == 'POST' and form.validate():
            message = form.message.data
            cur = mysql.connection.cursor()
            cur.execute("INSERT INTO messages(message, msg_by, msg_to) VALUES(%s, %s, %s)",
                        (message, session['id'], id))

            mysql.connection.commit()
            cur.close()
        return render_template('chat_room.html', form=form, friend=friend)
    else:
        return redirect(url_for('friends'))

@app.route('/chats', methods=['GET', 'POST'])
@is_logged_in
def chats():
    id = session['fid']
    cur = mysql.connection.cursor()
    cur.execute("SELECT message, msg_by, msg_to, username, msg_time FROM messages INNER JOIN \
                users ON messages.msg_by = users.id WHERE (msg_by = %s AND msg_to = %s) \
                OR (msg_by = %s AND msg_to = %s) ORDER BY messages.id ASC",
                (id, session['id'], session['id'], id))
    chats = cur.fetchall()

    cur.close()
    return render_template('chats.html', chats=chats)

#Form for filing application to sell item
class SellForm(Form):
    title = StringField('Title', [validators.DataRequired()])
    categories = SelectField('Categories', choices=categories)
    quantity = IntegerField('Quantity', [validators.DataRequired()])
    price_option = SelectField('Pricing', choices=[('Fixed', 'Fixed'), ('Ranged', 'Ranged')])
    price = DecimalField('Price', places=2, validators=[validators.DataRequired()])
    min_price = DecimalField('Min', places=2, validators=[validators.DataRequired()])
    max_price = DecimalField('Max', places=2, validators=[validators.DataRequired()])

def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

#file application to SU to sell item
@app.route('/sell_application', methods=['GET', 'POST'])
@is_logged_in
def sell_application():
    form = SellForm(request.form)
    if request.method == 'POST' and form.validate():
        title = form.title.data
        categories = form.categories.data
        quantity = form.quantity.data
        price_option = form.price_option.data
        price = form.price.data
        min_price = form.min_price.data
        max_price = form.max_price.data

        if 'file' not in request.files:
            flash('No File Part', 'danger')
        file = request.files.get('file')

        if file.filename == '':
            flash('No File Selected', 'danger')
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            print(filename)

        cur = mysql.connection.cursor()
        cur.execute("SELECT * FROM blocked_item")
        blocked_item = cur.fetchall()

        if title in blocked_item:
            flash('Your application is denied because the item is not appropriate', 'danger')
            return redirect(url_for('dashboard'))

        else:
            cur.execute("INSERT INTO Products(title, categories, filename, quantity, \
                        price_option, status, owner) VALUES(%s, %s, %s, %s, %s, %s, %s)",
                        (title, categories, filename, quantity, price_option, 'pending', session['username']))
            
            cur.execute("SELECT LAST_INSERT_ID()")
            product_id = cur.fetchone()

            if(price_option == "Ranged"):  
                cur.execute("INSERT INTO ranged_price(id, min_price, max_price, highestBid, secondHighestBid) VALUES(%s, %s, %s, %s, %s)", \
                            (product_id['LAST_INSERT_ID()'], min_price, max_price, min_price, min_price))
            else:
                cur.execute("INSERT INTO fixed_price(id, price) VALUES(%s, %s)", \
                            (product_id['LAST_INSERT_ID()'], price))

            mysql.connection.commit()
            cur.close()

            flash('Your application is proccessing. Please wait...', 'success')
            return redirect(url_for('dashboard'))
    return render_template('sell_application.html', form=form) 

# Process item information user submitted to sell an item
@app.route('/process_item_info')
@is_logged_in
def process_item_info():
    cur = mysql.connection.cursor()

    cur.execute("SELECT * FROM Products WHERE Products.status = 'pending'")
    applications = cur.fetchall()
    cur.close()

    if applications:
        return render_template('process_item_info.html', applications=applications)
    else:
        msg = 'No Application Found'
        return render_template('process_item_info.html', msg=msg)
    
class JustificationForm(Form):
    justification = TextAreaField('Justification', [validators.DataRequired()])

#reject item application
@app.route('/reject_item/<string:id>', methods=['POST'])
@is_logged_in
def reject_item(id):
    form = JustificationForm(request.form)
    if request.method == 'POST' and form.validate():
        justification = form.justification.data
        cur = mysql.connection.cursor()
        cur.execute("UPDATE Products SET status = 'rejected', reason = %s WHERE \
                    id = %s", (justification, id))

        cur.execute("SELECT users.id FROM Products INNER JOIN users ON username = owner \
                    WHERE Products.id = %s", [id])
        user = cur.fetchone()
        send_warning(user['id'])

        mysql.connection.commit()
        cur.close()

        flash('Application Rejected', 'success')
        return redirect(url_for('process_item_info'))
    return render_template('justification.html', form=form)

#approve item application
@app.route('/approve_item/<string:id>', methods=['POST'])
@is_logged_in
def approve_item(id):
    cur = mysql.connection.cursor()

    now = datetime.datetime.now()
    cur.execute("UPDATE Products SET status = 'selling' WHERE id = %s", [id])
    cur.execute("UPDATE ranged_price SET start_time = %s, end_time = %s WHERE id = %s", \
                (now.strftime("%Y-%m-%d %H:%M:%S"), now + timedelta(days=7), id))

    mysql.connection.commit()
    cur.close()

    flash('Application Approved', 'success')
    return redirect(url_for('process_item_info'))

#show item status
@app.route('/item_status')
@is_logged_in
def item_status():
    cur = mysql.connection.cursor()
    cur.execute("SELECT * FROM Products WHERE (Products.status = 'selling' OR \
        Products.status = 'rejected' OR Products.status = 'pending') AND owner = %s", [session['username']])
    items = cur.fetchall()

    if items:
        return render_template('item_status.html', items=items)
    else:
        msg = "No item found."
        return render_template('item_status.html', msg=msg)
    cur.close()

#sell item
@app.route('/sell_item', methods=['GET'])
@is_logged_in
def sell_item():
    cur = mysql.connection.cursor()
    cur.execute("SELECT * FROM products INNER JOIN transact ON products.id = transact.product_id WHERE transact.status = 'pending'")
    fixed_items = cur.fetchall()

    cur.execute("SELECT * FROM products INNER JOIN fixed_price\
                ON fixed_price.id = products.id \
                INNER JOIN ranged_price ON ranged_price.id = products.id")
    ranged_items = cur.fetchall()

    if fixed_items or ranged_items:
        return render_template('sell_item.html', fixed_items=fixed_items, ranged_items=ranged_items)
    else:
        msg = "No item found."
        return render_template('sell_item.html', msg=msg)
    cur.close()

@app.route('/sell',methods=['POST'])
@is_logged_in
def sell():
    cur = mysql.connection.cursor()
    cur.close()

#show sold items(unfinished !!!)
@app.route('/sold_items')
@is_logged_in
def sold_items():
    cur = mysql.connection.cursor()
    cur.execute("SELECT * FROM Products INNER JOIN ranged_price ON Products.id = ranged_price.id \
        INNER JOIN fixed_price ON Products.id = fixed_price.id WHERE (Products.status = 'sold') \
        AND owner = %s", [session['username']])
    items = cur.fetchall()

    if items:
        return render_template('sold_items.html', items=items)
    else:
        msg = "No item found."
        return render_template('sold_items.html', msg=msg)
    cur.close()

#show items that are not sold by pass bidding end date
@app.route('/unsold_items')
@is_logged_in
def unsold_items():
    cur = mysql.connection.cursor()
    cur.execute("SELECT * FROM Products WHERE Products.status = 'unsold' AND owner = %s", [session['username']])
    items = cur.fetchall()

    if items:
        return render_template('unsold_items.html', items=items)
    else:
        msg = "No item found."
        return render_template('unsold_items.html', msg=msg)
    cur.close()

#Show transaction history
@app.route('/transaction_history', methods=['GET'])
@is_logged_in
def transaction_hist():
    cur = mysql.connection.cursor()

    cur.execute("SELECT transact.*, products.title FROM transact \
                INNER JOIN products ON transact.product_id = products.id \
                WHERE seller_id = %s OR buyer_id = %s", (session['id'], session['id']))
    transact = cur.fetchall()

    cur.execute("SELECT * FROM users WHERE permission = 1")
    users = cur.fetchall()

    cur.execute("SELECT * FROM products")
    products = cur.fetchall()

    cur.close()
    return render_template('transaction_history.html', transact=transact, users=users, products=products)

#SU side transaction statistics
@app.route('/transaction_stat', methods=['GET'])
@is_logged_in
def transaction_stat():
    cur = mysql.connection.cursor()

    cur.execute("SELECT * FROM transact")
    stat = cur.fetchall()

    cur.close()
    return render_template('transaction_stat.html', stat=stat)

class RatingForm(Form):
    rating = RadioField('Rating', choices=[(0, '0'), (1, '1'), (2, '2'), (3, '3'), (4, '4'), (5, '5')])

@app.route('/rate/<string:id>', methods=['POST'])
@is_logged_in
def rate_seller(id):
    form = RatingForm(request.form)
    if request.method == 'POST' and form.validate():
        rating = form.rating.data
        cur = mysql.connection.cursor()
        cur.execute("UPDATE transact SET rating = %s WHERE \
                    buyer_id = %s", (rating, id))
        mysql.connection.commit()
        cur.close()

        flash('Rating completed', 'success')
        return redirect(url_for('order_details'))
    return render_template('rate.html')

@app.route('/order_details')
@is_logged_in
def order_details():
    cur = mysql.connection.cursor()
    cur.execute("SELECT * FROM transact INNER JOIN products ON transact.product_id = \
        products.id WHERE buyer_id = %s", [session['id']])
    items = cur.fetchall()

    cur.execute("SELECT * FROM complaint WHERE product_id IN (SELECT product_id FROM transact)")
    complaints = cur.fetchall()

    if items:
        return render_template('order_details.html', items=items, complaints=complaints)
    else:
        msg = "No item found."
        return render_template('order_details.html', msg=msg)
    cur.close()

#show notifications to user(purchasing intention item is up etc...)
@app.route('/show_notifications')
@is_logged_in
def show_notifications():
    cur = mysql.connection.cursor()
    cur.execute("SELECT * FROM notification WHERE receiver = %s", [session['id']])
    notifications = cur.fetchall()
    cur.close()

    return render_template('show_notifications.html', notifications=notifications)

#show content of notification after subject is clicked
@app.route('/show_notifications/<string:id>')
@is_logged_in
def show_notification_content(id):
    cur = mysql.connection.cursor()
    cur.execute("SELECT * FROM notification WHERE id = %s", [id])
    notification = cur.fetchone()

    cur.close()
    return render_template('notification.html', notification=notification)

#appeal for warning
@app.route('/appeal', methods=['POST'])
def appeal():
    form = JustificationForm(request.form)
    if request.method == 'POST' and form.validate():
        justification = form.justification.data
        cur = mysql.connection.cursor()

        cur.execute("UPDATE user_status SET status = %s", ['appealed'])
        cur.execute("INSERT INTO appeal(user_id, reason) VALUES(%s, %s)", (session['id'], justification))

        mysql.connection.commit()
        cur.close()

        flash('Application Filed', 'success')
        return redirect(url_for('login'))
    return render_template('justification.html', form=form)

#resign -> removed from the system
@app.route('/resign')
def resign():
    cur = mysql.connection.cursor()
    cur.execute("SELECT * FROM userprofile WHERE id = %s", [session['id']])
    user = cur.fetchone()
    cur.execute("DELETE FROM users WHERE id = %s", [session['id']])

    cur.execute("INSERT INTO blocked_user(firstname, lastname) VALUES(%s, %s)",
                (user['firstname'], user['lastname']))

    mysql.connection.commit()
    cur.close()

    return redirect(url_for('login'))

# Process appeal
@app.route('/process_appeal')
@is_logged_in
def process_appeal():
    cur = mysql.connection.cursor()

    cur.execute("SELECT appeal.id, appeal.user_id, appeal.reason, userprofile.firstname, \
        userprofile.lastname FROM appeal INNER JOIN userprofile ON appeal.user_id = userprofile.id")
    applications = cur.fetchall()

    cur.close()

    if applications:
        return render_template('process_appeal.html', applications=applications)
    else:
        msg = 'No Application Found'
        return render_template('process_appeal.html', msg=msg)

#approve appeal
@app.route('/approve_appeal/<string:id>', methods=['POST'])
@is_logged_in
def approve_appeal(id):
    cur = mysql.connection.cursor()
    
    cur.execute("UPDATE user_status SET status = 'OU' WHERE id = %s", (id))
    cur.execute("DELETE FROM appeal WHERE user_id = %s", (id))

    mysql.connection.commit()
    cur.close()

    flash('Appeal Approved', 'success')
    return redirect(url_for('process_appeal'))

class TabooForm(Form):
    word = StringField('', [validators.DataRequired()])

#Suggest taboo word
@app.route('/suggest_taboo', methods=['GET', 'POST'])
@is_logged_in
def suggest_taboo():
    form = TabooForm(request.form)
    if request.method == 'POST' and form.validate():
        word = form.word.data
        cur = mysql.connection.cursor()

        #A OU suggest a taboo word
        if session['permission'] == 1:
            cur.execute("INSERT INTO taboo(word, status) VALUES(%s, %s)", (word, 'pending'))
            mysql.connection.commit()
            cur.close()

            flash('The word is reported. Please wait...', 'success')
            return redirect(url_for('dashboard'))

        # A SU adds a word to taboo list
        else:
            cur.execute("INSERT INTO taboo(word, status) VALUES(%s, %s)", (word, 'approved'))
            mysql.connection.commit()
            cur.close()

            flash('A taboo word is added', 'success')
            return redirect(url_for('list_taboo'))
        
    return render_template('suggest_taboo.html', form=form)

# Taboo list
@app.route('/list_taboo')
@is_logged_in
def list_taboo():
    cur = mysql.connection.cursor()

    cur.execute("SELECT * FROM taboo ORDER BY status ASC")
    words = cur.fetchall()
    cur.close()

    if words:
        return render_template('list_taboo.html', words=words)
    else:
        msg = 'No Taboo Words Found'
        return render_template('list_taboo.html', msg=msg)

# Delete taboo word from the list
@app.route('/delete_taboo/<string:id>', methods=['POST'])
@is_logged_in
def delete_taboo(id):
    cur = mysql.connection.cursor()
    cur.execute("DELETE FROM taboo WHERE id = %s", (id))

    mysql.connection.commit()
    cur.close()

    flash('Action Success', 'success')
    return redirect(url_for('list_taboo'))

#approve taboo words
@app.route('/approve_taboo/<string:id>', methods=['POST'])
@is_logged_in
def approve_taboo(id):
    cur = mysql.connection.cursor()
    cur.execute("UPDATE taboo SET status = 'approved' WHERE id = %s", (id))

    mysql.connection.commit()
    cur.close()

    flash('Approved', 'success')
    return redirect(url_for('list_taboo'))

# check if  the string contains taboo word
def check_taboo(str):
    with app.app_context():
        cur = mysql.connection.cursor()
        cur.execute("SELECT * FROM taboo")
        words = cur.fetchall()
        cur.close()

        for word in words:
            if word['word'] in str: 
                return "***"
        return str

#send warning
def send_warning(id):
    with app.app_context():
        cur = mysql.connection.cursor()
        cur.execute("SELECT * FROM user_status WHERE id= %s", [id])
        user_status = cur.fetchone()
        warning = user_status['warning'] + 1
        now = datetime.datetime.now()

        if warning < 2:
            cur.execute("UPDATE user_status SET warning = %s WHERE id = %s", (warning, id))

        else:
            cur.execute("UPDATE user_status SET warning = %s, status = %s WHERE id = %s", \
                        (warning, 'suspended', id))
        
        cur.execute("INSERT INTO notification(sender, subject, content, received_date, receiver) \
            VALUES(%s, %s, %s, %s, %s)", ('SimpleMY', 'WARNING!!!', "You received a warning \
            because of rejected item application. Keep in mind that if you receive two warnings, \
            you will be suspended from the system.", now.strftime("%Y-%m-%d %H:%M:%S"), id))

        mysql.connection.commit()
        cur.close()

# list complaints
@app.route('/list_complaints')
@is_logged_in
def list_complaints():
    cur = mysql.connection.cursor()
    
    cur.execute("SELECT complaint.*, users.username FROM complaint INNER JOIN users ON \
        users.id = complaint.sender WHERE receiver = %s and status = 'resolving'", [session['id']])
    complaints = cur.fetchall()
    cur.close()

    if complaints:
        return render_template('list_complaints.html', complaints=complaints)
    else:
        msg = 'No Complaints Found'
        return render_template('list_complaints.html', msg=msg)

class ComplaintForm(Form):
    body = TextAreaField('Complaint', [validators.DataRequired()])

#file complaint
@app.route('/file_complaint', methods=['GET', 'POST'])
def file_complaint(id):
    form = ComplaintForm(request.form)
    if request.method == 'POST' and form.validate():
        body = form.body.data
        cur = mysql.connection.cursor()

        cur.execute("INSERT INTO complaint(body, sender, receiver, status) VALUES(%s, %s, %s, %s)", \
                    (body, session['id'], id, 'pending'))

        mysql.connection.commit()
        cur.close()

        flash('Complaint Filed', 'success')
        return redirect(url_for('order_details.html'))
    return render_template('file_complaint.html', form=form)

#respond to complaint
@app.route('/respond_complaint<string:id>', methods=['GET', 'POST'])
def respond_complaint(id):
    form = ComplaintForm(request.form)
    if request.method == 'POST' and form.validate():
        response = form.response.data
        cur = mysql.connection.cursor()

        cur.execute("UPDATE complaint SET response = %s status = %s WHERE sender = %s AND \
            receiver = %s", (response, id, session['id'], 'resolved'))

        mysql.connection.commit()
        cur.close()

        flash('Complaint Filed', 'success')
        return redirect(url_for('list_complaint'))
    return render_template('list_complaint.html', form=form)

# Process complaint
@app.route('/process_complaint')
@is_logged_in
def process_complaint():
    cur = mysql.connection.cursor()

    cur.execute("SELECT complaint.*, users.username FROM complaint INNER JOIN users ON \
        users.id = complaint.receiver WHERE status = 'pending' OR status = 'resolved'")
    complaints = cur.fetchall()
    cur.close()

    if complaints:
        return render_template('process_complaint.html', complaints=complaints)
    else:
        msg = 'No Complaint Found'
        return render_template('process_complaint.html', msg=msg)

# Reject complaint and delte it from db
@app.route('/reject_complaint/<string:id>', methods=['POST'])
@is_logged_in
def reject_complaint(id):
    cur = mysql.connection.cursor()
    cur.execute("DELETE FROM complaint WHERE id = %s", (id))

    mysql.connection.commit()
    cur.close()

    flash('Complaint Rejected', 'success')
    return redirect(url_for('list_complaints'))

#approve complaint
@app.route('/approve_complaint/<string:id>', methods=['POST'])
@is_logged_in
def approve_complaint(id):
    cur = mysql.connection.cursor()
    cur.execute("UPDATE complaint SET status = 'approved' WHERE id = %s", (id))

    mysql.connection.commit()
    cur.close()

    flash('Approved', 'success')
    return redirect(url_for('list_complaints'))

#communiate with receiver for the complaint
@app.route('/resolve_complaint/<string:id>', methods=['POST'])
@is_logged_in
def resolve_complaint(id):
    cur = mysql.connection.cursor()
    cur.execute("UPDATE complaint SET status = 'resolving' WHERE id = %s", (id))

    mysql.connection.commit()
    cur.close()

    flash('Complaint Sent to Receiver', 'success')
    return redirect(url_for('list_complaints'))

# list all items on sale
@app.route('/list_items')
@is_logged_in
def list_items():
    cur = mysql.connection.cursor()

    cur.execute("SELECT * FROM Products WHERE status = 'selling'")
    items = cur.fetchall()
    cur.close()

    if items:
        return render_template('list_items.html', items=items)
    else:
        msg = 'No Item Found'
        return render_template('list_items.html', msg=msg)

# Delete item and blacklist it
@app.route('/delete_item/<string:id>', methods=['POST'])
@is_logged_in
def delete_item(id):
    cur = mysql.connection.cursor()
    cur.execute("SELECT * FROM Products WHERE id = %s", (id))
    product = cur.fetchone()

    cur.execute("DELETE FROM Products WHERE id = %s", (id))
    cur.execute("INSERT INTO blocked_item(title) VALUES(%s)", (product['title']))

    mysql.connection.commit()
    cur.close()

    flash('Item Deleted', 'success')
    return redirect(url_for('list_items'))

if __name__ == "__main__": 
    app.secret_key='secret123'
    app.run(debug=True)
