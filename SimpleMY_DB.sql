CREATE DATABASE SimpleMy;

CREATE TABLE users(
    id INT NOT NULL AUTO_INCREMENT,
    username VARCHAR(255),
    password VARCHAR(255),
    permission INT,
    PRIMARY KEY(id, username),
    UNIQUE KEY unique_username (username)
);

CREATE TABLE userprofile(
    id INT NOT NULL PRIMARY KEY,
    firstname VARCHAR(255),
    lastname VARCHAR(255),
    email VARCHAR(255),
    phone_number VARCHAR(255),
    address VARCHAR(255),
    city VARCHAR(255),
    state VARCHAR(255),
    zipcode INT(5),
    credit_card_number VARCHAR(255),
    cvc INT,
    expiration VARCHAR(255),
    FOREIGN KEY (id) REFERENCES users(id) on UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE user_relation(
    user_first_id INT NOT NULL,
    user_second_id INT NOT NULL,
    relationship VARCHAR(255),
    PRIMARY KEY(user_first_id, user_second_id),
    FOREIGN KEY (user_first_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (user_second_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE,
    CHECK (user_first_id < user_second_id)  
);

INSERT INTO users(id, username, password, permission) VALUES (1, "admin", "admin", 0);

CREATE TABLE Products (
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    title VARCHAR (255),
    categories VARCHAR(255),
    filename VARCHAR (255),
    quantity INT,
    price_option VARCHAR(255),
    status VARCHAR (255),
    reason VARCHAR(255),
    owner VARCHAR(255),
    FOREIGN KEY (owner) REFERENCES users(username) ON UPDATE CASCADE ON DELETE CASCADE);

CREATE TABLE fixed_price(
    id INT NOT NULL PRIMARY KEY,
    price DECIMAL(7,2),
    FOREIGN KEY (id) REFERENCES Products(id) ON UPDATE CASCADE ON DELETE CASCADE);

CREATE TABLE ranged_price(
    id INT NOT NULL PRIMARY KEY,
    min_price DECIMAL(7,2),
    max_price DECIMAL(7,2),
	highestBid	decimal(7,2),
	secondHighestBid decimal(7,2),
    start_time VARCHAR(255),
    end_time VARCHAR(255),
    FOREIGN KEY (id) REFERENCES Products(id) ON UPDATE CASCADE ON DELETE CASCADE);


CREATE TABLE messages(
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    message VARCHAR (255),
    msg_by VARCHAR (255),
    msg_to VARCHAR (255),
    msg_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP);

CREATE TABLE user_status(
    id INT NOT NULL PRIMARY KEY,
    rating INT,
    money_spent Decimal(7,2) NOT NULL DEFAULT 0.00,
    warning INT NOT NULL DEFAULT 0,
    status VARCHAR(255),
    FOREIGN KEY (id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE);

CREATE TABLE blocked_user(
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    firstname VARCHAR (255),
    lastname VARCHAR (255));
    
CREATE TABLE transact (
    id int(11) NOT NULL AUTO_INCREMENT,
    seller_id int(11) NOT NULL,
    buyer_id int(11) NOT NULL,
    product_id int(11) NOT NULL,
    quantity INT NOT NULL,
    price decimal(7,2) NOT NULL DEFAULT '99999.99',
    status varchar(255) DEFAULT NULL,
    time timestamp NULL DEFAULT CURRENT_TIMESTAMP,
    rating INT(1)
    CHECK (rating <= 5 AND rating >= 0),
    PRIMARY KEY (id),
    KEY product_id (product_id),
    CONSTRAINT transact_ibfk_1 FOREIGN KEY (product_id) REFERENCES products (id) ON DELETE CASCADE ON UPDATE CASCADE);

CREATE TABLE notification (
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    sender VARCHAR(255),
    receiver INT,
    subject VARCHAR(255),
    content VARCHAR(1024),
    item_ends VARCHAR(255),
    received_date VARCHAR(255),
    status VARCHAR(255),
    FOREIGN KEY (receiver) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE);

CREATE TABLE appeal(
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    user_id INT NOT NULL,
    reason VARCHAR(1024),
    FOREIGN KEY (user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE);

CREATE TABLE taboo(
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    word VARCHAR(255),
    status VARCHAR(255));

CREATE TABLE complaint(
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    body VARCHAR(1024),
    response VARCHAR(1024),
    sender INT NOT NULL,
    receiver INT NOT NULL,
    product_id INT,
    status VARCHAR(255),
    FOREIGN KEY (product_id) REFERENCES Products(id) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (sender) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (receiver) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE);

CREATE TABLE blocked_item(
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    title VARCHAR (255));
