# CSc 322 - SimpleMY

## To get the application up and running...
- Install MySQL
- Create a db user named: `csc322` with password `csc322`
- Create a database named: `SimpleMY`
- Clone this repository
- `cd csc322`
- Create a virtual environment and activate it:
- `python -m venv venv`
- `$ . venv/bin/activate`
- Install dependencies with:
- `pip install -r requirements.txt`
- Run the app:
- `python app.py`
- visit: http://localhost:5000/